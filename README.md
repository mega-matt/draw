# Whiteboard web app
This web app provides a canvas for drawing shapes, writing text, and more. 
Demo: [drawzone.net](https://drawzone.net)

# Development
### Local 
```
yarn install
yarn start
```
### Deploy
Pipelines are hooked up to develop and master and deployed on pushes. 
> develop = beta.drawzone.net

> master drawzone.net

# Project Setup
This is a react + redux + react-redux-simple-store app.

It consists of a canvas which the user can draw and add text and images and collaborate
live with other users. It is highly a work in progress so there is a long todo list. 

## Store Structures
There are two main stores (main, remote) each with a few namespaces. Main tracks one's own drawings while remote 
tracks the events from other users connected to a session. 

### Tools
Tools are the name given to the types of things that can go into the canvas. Eg. free-hand, line, polygon, text, picture.

Mouse and keyboard events are sent to the main store which determines the particular tool and 
dispatches an appropriate event. Each tool should implement a mouse-down, mouse-move, and mouse-up handler (keyboard handler coming soon).

### Shapes
Tools make "shapes". Shapes ecompasses anything actually in a canvas made from a tool including text and images. 
There is a differentiation between the currently active shape "liveShape" and the visible shape list. The canvas
subscribes to both and renders each shape according to a component that matches the shape tool type.

### Remote
WebRTC is used for connecting people to a session. There are components that when placed into the dom
will monitor changes to the main db hooks and make remote dispatch statements. The remote dispatch statements 
ultimately are responsible for transiting the data to the remotely connected users. It tries to optimistically send new / removed shapes and relies upon a shape serialization hash to sync up missed / dropped messages.

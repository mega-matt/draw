export function Avatar({nickname, size, className = ''}: {nickname: string|undefined, size: number, className?: string}) {
    const nick = nickname?.toLowerCase().includes('megamatt') ? 'Mega' : nickname;
    return <img alt='user gify' className={className} src={`https://avatars.dicebear.com/api/bottts/${nick}.svg?size=${size}`} />
}
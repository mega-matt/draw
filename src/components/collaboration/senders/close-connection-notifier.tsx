import { useEffect } from "react";
import { dispatcher } from "../../../store/remote";

export function CloseConnectionNotifier() {
    useEffect(() => {
        // no need to unsub, window is unloading
        window.addEventListener('beforeunload', dispatcher(':message/->send/disconnect'))
    }, [])
    return null;
}
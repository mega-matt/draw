import React, { ReactNode } from 'react';
import { useColabConnected } from '../../store/hooks';

export function WhenCollaborationConnected({ children }: { children: ReactNode }) {
    const isConnected = useColabConnected();

    return isConnected
        ? <>{children}</>
        : null;
}
import { useWelcomeVisable } from '../store/hooks';
import './welcome.css';
export const Welcome = () => useWelcomeVisable()
    ? <div className='welcome'>Draw Here</div>
    : null;
import { useState } from "react"

export const useWithPrevState = <T>(initVal?: T): [[state: T|undefined, prevValue: T|undefined], (newVal: T) => any] => {
    const [state, setState] = useState(initVal);
    const [prev, setPrev] = useState<T>();

    const set = (newVal: T) => {
        setPrev(state);
        setState(newVal);
    }

    return [[state, prev], set]
}
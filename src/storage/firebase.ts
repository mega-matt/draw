import { initializeApp } from "firebase/app";
// import { getFirestore, collection, getDocs, getDoc, addDoc } from "firebase/firestore";
import { getDatabase, ref, onValue, set } from 'firebase/database';
import { useEffect, useState } from "react";


export const firebaseConfig = {
  apiKey: "AIzaSyDPbBwSYDqETOemKsVPjLaypALL5pViczM",
  authDomain: "white-board-app-cf8df.firebaseapp.com",
  projectId: "white-board-app-cf8df",
  storageBucket: "white-board-app-cf8df.appspot.com",
  messagingSenderId: "585139684616",
  databaseURL: "https://white-board-app-cf8df-default-rtdb.firebaseio.com",
  appId: "1:585139684616:web:6bc12c6c0ba23870f518bd"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const fs = getFirestore(app);
const db = getDatabase(app);

const refCounter: Record<string, { unsub?: any, setters: ((next: any) => any)[] }> = {}

// export const subscribe = (path: string, handler: (v: any) => any) => {
//     return onValue(ref(db, path), s => handler(s.val()));
// }

export const useFbState = <T = any>(path: string) => {
    const [val, setVal] = useState<T|undefined>();
    
    useEffect(() => {
        refCounter[path] = refCounter[path] || {
            unsub: onValue(ref(db, path), s => { refCounter?.[path]?.setters.forEach(setter => setter(s.val())); }),
        };
        refCounter[path].setters.push(setVal);

        return () => {
            refCounter[path].setters = refCounter[path].setters.filter(s => s !== setVal);
            if (!refCounter[path].setters.length) {
                refCounter[path].unsub();
                delete refCounter[path]
            }
        }
    }, []);
    
    return [val, setVal]
}

export const setFb = async (path: string, val: any) => {
    await set(ref(db, path), val)
}


// export const getRandomId = () => collection({} as any, 'test').

import { ReactNode } from 'react';
import { iNotificationConfig } from '../components/notification-drawer';
import { Shape, Settings } from '../tools';

export type iSnapshot = {
    thumb: string,
    full: string,
    time: number,
    shapes: Shape[],
}
export type WhiteBoard = {
    settings: Settings,
    visibleActionMenu?: string;

    nickname?: string;
    rename?: boolean;
    lastXY?: [number, number];

    shapes: {
        vis: Shape[],
        undid: Shape[],
        live?: Shape,
    },

    highlightedUser?: string,
    hiddenRemoteUsers: Record<string, boolean>,

    notificationDrawer?: {
        body: ReactNode,
        config: iNotificationConfig,
    },

    snapshotTap?: any,
    snapshots: iSnapshot[],
    t: '',
}
export const eventXy = (evt: MouseEvent): [number, number] => {
    const x = !evt.offsetX ? (evt as any).layerX : evt.offsetX;
    const y = !evt.offsetY ? (evt as any).layerY : evt.offsetY;
    return [x, y];
}
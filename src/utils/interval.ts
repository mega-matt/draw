import { when } from "./when";

export function interval(fn: (...a: any[]) => any, ms: number, immediate: boolean = false) {
    when(immediate, fn);

    const idx = setInterval(fn, ms);
    return () => clearInterval(idx);
}
export type Minus<n extends number> = n extends 10 ? 9 
: n extends 9 ? 8
: n extends 8 ? 7
: n extends 7 ? 6
: n extends 6 ? 5
: n extends 5 ? 4
: n extends 4 ? 3
: n extends 3 ? 2
: n extends 2 ? 1
: 0;
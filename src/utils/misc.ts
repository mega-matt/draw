export const isMac = navigator.userAgent.includes('Mac');
export const ctrlKey = isMac ? 'Cmd' : 'Ctrl';
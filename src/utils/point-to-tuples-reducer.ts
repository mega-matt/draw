export const pointsToTuples = (p: any[], n: any, ...any: any[]): [number, number][] => {
    const lastIdx = p.length - 1;
    if (p[lastIdx]?.length === 1) p[lastIdx].push(n);
    else p.push([n])
    return p;
}
import { Shape } from '../tools';
import md5 from 'md5';

export function shapesHasher(shapes?: Shape[]) {
    return !shapes ? 'nope' : md5(JSON.stringify(shapes));
}
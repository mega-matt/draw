export function timeout(fn: (...any: any[]) => any, ms: number) {
    const idx = setTimeout(fn, ms);
    return () => clearTimeout(idx);
}
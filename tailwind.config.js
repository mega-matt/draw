const colors = {
  highlight: 'teal',
  green: 'limegreen',
}
module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      white: '#fff',
      black: '#333',
      grey: {
        DEFAULT: "#8a8a8a",
        100: '#636363',
        75: '#8a8a8a',
        50: '#b1b1b1',
        25: '#d8d8d8',
        15: '#e8e8e8',
        10: '#f3f3f3'
      }
    },
    extend: {
      cursor: {
        none: 'none',
      },
      colors: colors,
      textColor: colors,
      borderColor: colors,
      // fontFamily: {
      //   ...fontFamily,
      // },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
